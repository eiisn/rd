$('#login-form').submit(function (e) {
    let id_form = $(this).attr('id');
    let btn_submit = $(this).find('input[type=submit]');
    $('#no-user-error').css('display', 'none');
	$('#password-error').css('display', 'none');
    btn_submit.prop('disabled', true);
    e.preventDefault();
    $.ajax({
        url: "/accounts/ajax_login/",
        type: "POST",
        data: $(this).serialize(),
        success: function (data) {
            console.log(JSON.stringify(data));
            let login_response = data;
            if (login_response.user === "no user") {
                $('#no-user-error').css('display', 'block');
                btn_submit.prop('disabled', false);
            } else if (login_response.user === "wrong password") {
                $('#password-error').css('display', 'block');
                btn_submit.prop('disabled', false);
            } else if ((login_response.user === "not active") && (login_response.user_phone)) {
                alert("Compte désactivé");
                btn_submit.prop('disabled', false);
            } else {
                if (login_response.login === "Failed") {
                    alert("Invalid Login!");
                } else {
                    let form = document.getElementById(id_form);
                    form.reset();
                    form.querySelector('#id_username').setAttribute('readonly', '');
                    form.querySelector('#id_password').setAttribute('readonly', '')
                    location.reload();
                }
            }
            btn_submit.prop('disabled', false);
        },
        error: function (data) {
            console.log(data);
            console.log(JSON.stringify(data, undefined, 2))
        }
    });
});

$('#close-login').on('click', () => {
    location.reload()
});
