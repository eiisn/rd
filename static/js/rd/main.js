jQuery.loadScript = function (url, callback) {
    jQuery.ajax({
        url: url,
        dataType: 'script',
        success: callback,
        async: true
    });
};


$(".nav-module").on('click', function (event) {
    event.preventDefault();
    const moduleTitle = $(this).text();
    const moduleName = $(this).attr('id').replace('-link', '');
    const moduleTab = document.getElementById(`tab-${moduleName}`);
    document.title = moduleName.charAt(0).toUpperCase() + moduleName.slice(1);
    if (!moduleTab) {
        $('<li class="nav-item">' +
            `<a class="nav-link closable" id="tab-${moduleName}" data-toggle="tab" href="#${moduleName}-tab" role="tab" aria-controls="${moduleName}" aria-selected="true">` +
            `   ${moduleTitle} <i class="fas fa-times close-link"></i>` +
            '</a>' +
            '</li>').appendTo('#module-tab');
        $.ajax({
            url: `${$(this).prop('href')}`,
            type: "GET",
            success: function (data) {
                $(`<div class="tab-pane fade" id="${moduleName}-tab">${data.html}${data.formAdd}${data.formUpdate}</div>`).appendTo('#module-tab-content');
                initTabContent(moduleName, data);
                setClick();
                $('#module-tab li:last-child a').tab('show');
            },
            error: function (data) {
                $(`<div class="tab-pane" id="${moduleName}-tab">${data}</div>`).appendTo('#module-tab-content');
                setClick();
                $('#module-tab li:last-child a').tab('show');
            }
        });
    } else {
        $(moduleTab).tab('show');
    }
});

function initTabContent(moduleName, data) {
    let btnDelete = $(`#${moduleName}-btn-delete`);
    let btnEdit = $(`#${moduleName}-btn-edit`);
    let formAdd = $(`#${moduleName}-form-add`);
    let formUpdate = $(`#${moduleName}-form-update`);
    let toolbar = $(`#${moduleName}-datatable-toolbar`);
    let tableElem = $(`#${moduleName}-table`);
    let table = setTable(tableElem, toolbar, btnEdit, btnDelete, data.options);
    let btnSync = $(`#${moduleName}-btn-sync`);
    if (formAdd.length)
        setAddForm(formAdd, table, btnSync, data.options);
    if (formUpdate.length)
        setUpdateForm(formUpdate, table, btnSync, data.options);
    setBtnEdit(btnEdit, table, formUpdate);
    setBtnDelete(btnDelete, table, data.options);
    if (data.script) {
        $.loadScript(data.script, function (e) {
            console.log(moduleName + " Load custom script");
        })
    }
}

function setTable(tableElem, toolbar, btnEdit, btnDelete, options) {
    options.columnDefs[0].render = columnRender;
    options.buttons[2].action = reloadAjax;
    let table = tableElem.DataTable(options);
    table.buttons().container().appendTo(toolbar);
    toolbar.find('.dt-buttons').addClass('btn-group');
    table.on('select', function () {
        btnDelete.addClass('bg-warning').removeClass('disabled');
        btnEdit.addClass('bg-success').removeClass('disabled');
        btnDelete.prop("disabled", false);
        btnEdit.prop("disabled", false)
    });
    table.on('deselect', function () {
        btnDelete.addClass('disabled').removeClass('bg-warning');
        btnEdit.addClass('disabled').removeClass('bg-success');
        btnDelete.prop("disabled", true);
        btnEdit.prop("disabled", true)
    });
    return table;
}

function setAddForm(form, table, btnSync, options) {
    form.submit(function (event) {
        event.preventDefault();
        let data = getFormData(this, "add-");
        let btn_submit = $(this).find('input[type=submit]');
        btn_submit.prop('disabled', true);
        $.ajax({
            url: options.ajax,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: (data) => {
                $(this).parent('.modal').modal('hide');
                btn_submit.prop('disabled', false);
                reloadAjax(null, table);
                $(this)[0].reset();
            },
            error: (data) => {
                console.error(data);
                btn_submit.prop('disabled', false);
                alert("Une erreur s'est produite pendant l'ajout.")
            }
        })
    });
}

function setUpdateForm(form, table, btnSync, options) {
    form.submit(function (event) {
        event.preventDefault();
        let rowData = table.row('.selected').data();
        let data = getFormData(this, "update-");
        let btn_submit = $(this).find('input[type=submit]');
        btn_submit.prop('disabled', true);
        data.append("id", rowData["id"]);
        $.ajax({
            url: options.ajax,
            type: 'PUT',
            data: data,
            processData: false,
            contentType: false,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
            },
            success: (data) => {
                $(this).parent('.modal').modal('hide');
                btn_submit.prop('disabled', false);
                reloadAjax(null, table);
                if ('esb' in data) {
                    console.log(data.esb)
                    let url = data.esb.url;
                    if (url) {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: JSON.stringify(data.esb),
                            success: (dataEsb) => {
                                console.log('response esb : ', dataEsb)
                                $.toast({
                                    title: "Synchronisé",
                                    content: "Produit envoyé en production",
                                    type: "success",
                                    delay: 5000
                                })
                            },
                            error: (dataEsb) => {
                                console.error('response esb : ', dataEsb)
                                alert("Une erreur s'est produite pendant la transmission à la production")
                            }
                        })
                    }
                }
            },
            error: (data) => {
                console.error(data);
                btn_submit.prop('disabled', false);
                alert("Une erreur s'est produite pendant la modification")
            }
        })
    })
}

function setBtnEdit(btn, table, editForm) {
    btn.prop("disabled", true)
    btn.on('click', function (event) {
        let data = table.row('.selected').data();
        if (typeof data !== "undefined") {
            for (let key in data) {
                if (data.hasOwnProperty(key)) {
                    let field = $(`#id_update-${key}`, editForm);
                    field.val(null);
                    switch (typeof data[key]) {
                        case "object" :
                            if (Array.isArray(data[key])) {
                                for (let k in data[key]) {
                                    let opt = $(`#id_update-${key} option`, editForm);
                                    opt.filter(function () {
                                        return $(this).html() === data[key][k];
                                    })
                                        .prop("selected", true);
                                }
                            } else {
                                if (data[key]) {
                                    field.val(data[key]["id"]);
                                } else {
                                    field.val(null);
                                }
                            }
                            break;
                        case "boolean":
                            field.prop('checked', data[key]);
                            break;
                        case "string":
                            if (key !== "piece") {
                                field.val(data[key]);
                            }
                            // ToDo réussir à intégrer le fichier existant dans la form d'édition
                            break;
                        default:
                            field.val(data[key])
                    }
                }
            }
        } else {
            event.preventDefault()
        }
    });
}

function setBtnDelete(btn, table, options) {
    btn.prop("disabled", true)
    btn.on('click', function () {
        let data = table.row('.selected').data();
        if (typeof data !== "undefined") {
            //console.log(`${options.ajax}${data.id}/`);
            $.ajax({
                url: `${options.ajax}${data.id}/`,

                beforeSend: function (xhr) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
                },
                contentType: 'application/json',
                type: 'DELETE',
                success: function (data) {
                    reloadAjax(null, table)
                },
                error: function (data) {
                    console.error(data);
                    alert('Une erreur s\'est produite pendant la suppression de l\'élément.')
                }
            })
        }
    });
}

function setSubTable(idTable, url) {
    $(`#${idTable} tbody`).on('click', 'td.details-control', function () {
        let table = $(`#${idTable}`).DataTable();
        let tr = $(this).closest('tr');
        let row = table.row(tr);
        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {
            row.child(format(row.data(), url)).show();
            tr.addClass('shown');
        }
    })
}


function format(rowData, url) {
    let div = $('<div/>').addClass('loading').text('Loading...');
    //console.log(rowData["id"]);
    //console.log(`${url}/${rowData['id']}`);
    $.ajax({
        url: `${url}/${rowData['id']}`,
        data: {},
        dataType: 'json',
        success: function (data) {
            div.html("");
            div.removeClass('loading');
            div.append(data.html);
            div.append(data.formAdd);
            div.append(data.formUpdate);
            initTabContent(`${data.module}`, data)
        },
        error: function (e) {
            console.log(e);
        }
    });
    return div;
}

function setClick() {
    /*
     * Set tab close click event
     */
    $("#module-tab").on('click', 'li a i.close-link', function (event) {
        let tabId = $(this).parent('a').attr("href");
        $(this).parents('li').remove('li');
        $(tabId).remove();
        $('#module-tab a:first').tab('show');
    });
    $('#module-tab').on('auxclick', 'li a.closable', function (event) {
        event.preventDefault();
        let tabId = $(this).attr('href');
        $(this).parent('li').remove('li');
        $(tabId).remove();
        $('#module-tab a:first').tab('show');
    });
    $('#module-tab .nav-link').on('click', function (e) {
        document.title = $(this).text();
    });
}

function getJSONFormData(form, prefix) {
    /*
     * Get data from form as Json.
     */
    let temp = $(form).serializeArray();
    let data = {};
    for (let i = 0; i < temp.length; i++) {
        data[temp[i]["name"].replace(prefix, '')] = temp[i]["value"]
    }
    return data
}

function getFormData(form, prefix) {
    /*
     * Get data form form
     */
    let data = new FormData(form);
    let newData = new FormData();
    for (let [name, value] of data) {
        newData.append(name.replace(prefix, ''), value)
    }
    return newData
}

function columnRender(data, type, row) {
    /*
     * Render a checkbox for boolean in Datatable.
     * Render img tag for images in Datatable.
     */
    if (type === 'export') {
        return data === true ? 'Yes' : 'No'
    } else if (type === 'display') {
        if (typeof data === "boolean") {
            if (data === true) {
                return "<input type=\"checkbox\" class=\"editor-active\" onclick=\"return false;\" checked>"
            } else {
                return "<input type=\"checkbox\" onclick=\"return false;\" class=\"editor-active\" >"
            }
        } else if (data != null && data.includes("/media/")){
            if (data.includes(".pdf")){
                return "<embed src="+ data.toString() + " width=300 height=250 type='application/pdf'/>"
            }else {
                return "<img src="+ data.toString() + " style= 'max-height:150px; max-width:300px;' />"
            }
        } else if (Array.isArray(data)){
            return data.join(', ');
        } else {
            return data
        }
    } else {
        return data
    }
}

function reloadAjax(e, dt, node) {
    /*
     * Reload Datatable ajax when click on sync button
     */
    if (node)
        $(node).removeClass('pulse');
    dt.ajax.reload();
    dt.rows('.selected').deselect();
    $.toast({
        title: "Synchronisé",
        content: "Tableau à jour",
        type: "success",
        delay: 5000
    })
}

function getCookie(name) {
    /*
     * Django getCookie function :
     * Doc : https://docs.djangoproject.com/fr/3.0/ref/csrf/
     */
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

onready = () => {
    setClick()
};
