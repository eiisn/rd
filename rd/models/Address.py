from django.db import models


class Address(models.Model):
    active = models.BooleanField(default=True)
    invoice = models.BooleanField(default=False)
    delivery = models.BooleanField(default=False)
    building_name = models.CharField(max_length=255)
    street = models.CharField(max_length=255)
    zip = models.CharField(max_length=12)
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    longitude = models.DecimalField(max_digits=11, decimal_places=7, null=True)
    latitude = models.DecimalField(max_digits=11, decimal_places=7, null=True)

    class Meta:
        verbose_name = "Address"
        verbose_name_plural = "Addresses"

    def __str__(self):
        return self.address

    @property
    def address(self):
        return "%s %s, %s %s" % (self.street, self.zip, self.city, self.country)
