from rest_framework import serializers
from rd.models import Address


class AddressGETSerializer(serializers.ModelSerializer):
    address = serializers.ReadOnlyField()

    class Meta:
        model = Address
        fields = '__all__'


class AddressPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address
        fields = '__all__'
