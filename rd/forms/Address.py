from django import forms
from rd.models import Address


class AddressForm(forms.ModelForm):

    class Meta:
        model = Address
        exclude = ("id", )
