from rd.models import Address
from utils.BaseView import ERPView
from rd.serializers import AddressGETSerializer, AddressPOSTSerializer


class AddressView(ERPView):

    model = Address
    post_serializer = AddressPOSTSerializer
    get_serializer = AddressGETSerializer
