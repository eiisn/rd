from django.shortcuts import render, resolve_url


def main(request):
    side_link = {
        "product": {
            "name": "Product",
            "icon": "cubes",
            "modules": {
                "product": ["Product", resolve_url('product-index'), 'cubes'],
                "history": ["History", resolve_url('history-index'), 'history'],
            }
        },
        "ged": {
            "name": "GED",
            "icon": "industry",
            "modules": {
                "document": ["Document", resolve_url('document-index'), 'caret-right'],
            }
        },
        "research": {
            "name": "Research",
            "icon": "search",
            "modules": {
                "formula": ["Formula", resolve_url('formula-index'), 'infinity'],
                "scale": ["Scale", resolve_url('scale-index'), 'receipt'],
                "nomenclature": ["Nomenclature", resolve_url('nomenclature-index'), 'clipboard-list'],
                "container": ["Container", resolve_url('container-index'), 'spray-can'],
            }
        },
        "element": {
            "name": "Element",
            "icon": "puzzle-piece",
            "modules": {
                "substance": ["Actives Substances", resolve_url('substance-index'), 'atom'],
                "excipient": ["Excipients - Solvent", resolve_url('excipient-index'), 'flask'],
            }
        },
        "specs": {
            "name": "Specifications",
            "icon": "cogs",
            "modules": {
                "aim": ["Aims", resolve_url('aim-index'), 'crosshairs'],
                "effect": ["Effects", resolve_url('effect-index'), 'dizzy'],
                "excipient-category": ["Excipient Category", resolve_url('excipient-category-index'), 'bahai'],
                "galenic": ["Galenic", resolve_url('galenic-index'), 'burn'],
                "molecule": ["Molecules", resolve_url('molecule-index'), 'dna'],
                "origin": ["Origins", resolve_url('origin-index'), 'chart-pie'],
                "role": ["Roles", resolve_url('role-index'), 'bullseye'],
                "status": ["Status", resolve_url('status-index'), 'info'],
                "unit-of-measure": ["Unit of Measure", resolve_url('unit-of-measure-index'), 'square-root-alt'],
            }
        }
    }
    return render(request, "index.html", {
        "sidebar": side_link
    })
