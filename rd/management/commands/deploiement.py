from django.core.management.base import BaseCommand
from ged.models import Document, Visibility
from element.models import *
from research.models import *
from product.models import *
from django.contrib.auth.models import User
from django.core.files import File
from django.conf import settings

visibility = ("Equipe", "Privé", "Public", "Restreint")

users = (('agt', 'agt1agt1'),
         ('cbs', 'cbs2cbs2'),
         ('jan', 'jan3jan3'))

docs = (('cbs', 'Essai n°2 Formule 3', 'Image', 'Privé', 'DocTechnique.png'),
        ('jan', 'Essai n°8 Formule 14', 'Scan', 'Public', 'Breves.jpg'),
        ('cbs', 'Essai n°6 Formule 14', 'Pdf', 'Equipe', 'Notes.pdf'),
        ('jan', 'Essai n°4 Formule 10', 'Pdf', 'Public', 'File1.pdf'),
        ('agt', 'Essai n°3 Formule 8', 'JPG', 'Privé', 'Breves.jpg'),
        ('agt', 'Essai n°8 Formule 12', 'Pdf', 'Public', 'File1.pdf'),
        ('agt', 'Essai n°2 Formule 12', 'pnG', 'Restreint', 'DocTechnique.png'),
        ('jan', 'Essai n°4 Formule 14', 'jpg', 'Public', 'Breves.jpg'),
        ('cbs', 'Essai n°6 Formule 35', 'Pdf', 'Privé', 'Notes.pdf'),
        ('jan', 'Essai n°3 Formule 42', 'Pdf', 'Equipe', 'Notes.pdf'),
        ('cbs', 'Essai n°8 Formule 18', 'Pdf', 'Privé', 'File1.pdf'),
        ('jan', 'Essai n°8 Formule 26', 'Pdf', 'Public', 'Notes.pdf'),
        ('cbs', 'Essai n°4 Formule 33', 'Img', 'Equipe', 'DocTechnique.png'),
        ('jan', 'Essai n°6 Formule 41', 'Image', 'Public', 'Breves.jpg'),
        ('agt', 'Essai n°6 Formule 36', 'Pdf', 'Privé', 'Notes.pdf'),
        ('agt', 'Essai n°3 Formule 41', 'png', 'Public', 'DocTechnique.png'),
        ('agt', 'Essai n°4 Formule 15', 'Pdf', 'Restreint', 'Notes.pdf'),
        ('jan', 'Essai n°8 Formule 16', 'Pdf', 'Public', 'File1.pdf'),
        ('cbs', 'Essai n°2 Formule 5', 'Pdf', 'Privé', 'Notes.pdf'),
        ('jan', 'Essai n°8 Formule 7', 'Png', 'Equipe', 'DocTechnique.png'))

# atcCodes = (('A', 'Système digestif et métabolisme'), ('B', 'Sang et organes hématopoiétiques'),
#             ('C', 'Système cardio-vasculaire'), ('D', 'Dermatologie'),
#             ('G', 'Système génito-urinaire et hormones sexuelles'),
#             ('H', 'Hormones systémiques, à l\'exclusion des hormones sexuelles et des insulines'),
#             ('J', 'Anti-infectieux (usage systémique)'), ('L', 'Antinéoplasiques et agents immunomodulants'),
#             ('M', 'Système musculo-squelettique'), ('N', 'Système nerveux'),
#             ('P', 'Antiparasitaires, insecticides et répulsifs'), ('R', 'Système respiratoire'),
#             ('S', 'Organes sensoriels'), ('V', 'Divers'))

origin = ('Synthèse chimique', 'Substance naturelle', 'Substance issue du génie génétique', 'Synthèse hybride',
          'Extrait végétal', 'Extrait minéral')

aim = ('Antagonistes du calcium', 'Antalgique', 'Antiacides', 'Antiarythmiques', 'Antibactérienne',
       'Anticholinergiques', 'Anticoagulant', 'Antidépressive', 'Antidiarrhéiques', 'Antiépileptiques', 'Antifongique',
       'Antihistaminiques H1', 'Antihistaminiques H2', 'Antihypertenseurs', 'Anti-infectieuse', 'Anti-inflammatoire',
       'Antiparasitaire', 'Antiprotozoaire', 'Antipsychotiques', 'Antipyrétique', 'Antiseptique', 'Antispasmodique',
       'Anxiolytique', 'Calmante', 'Cardiotoniques', 'Carminative', 'Cicatrisante', 'Décontractant musculaire',
       'Diurétiques', 'Hypnotiques', 'Hypotensive', 'Musculotrope', 'Neurotonique', 'Sédative', 'Spasmolytique',
       'Tonique cardiaque', 'Vasodilatatrice')

moles = (('acide (2R,3Z,5R)-3-(2-hydroxyéthylidène)-7-oxo-4-oxa-1-azabicyclo[3.2.0]heptane-2-carboxylique', 'C8H9NO5'),
         ('3,7-diméthylocta-1,6-dién-3-ol', 'C10H18O.0'),
         ('1,3,3-triméthyl-2-oxabicyclo[2,2,2]octane', 'C10H18O.1'),
         ('1,7,7-Trimethylbicyclo[2.2.1]heptan-2-one', 'C10H16O.0'),
         ('acétate de 3,7-diméthylocta-1,6-dién-3-yle', 'C12H20O2.0'),
         ('(1R,4E,9S)-4,11,11-triméthyl-8-méthylidènebicyclo[7.2.0]undéc-4-ène', 'C15H24'),
         ('4-méthyl-1-(propan-2-yl)cyclohex-3-én-1-ol', 'C10H18O.2'),
         ('(Z)-3,7-diméthyl-1,3,6-octatriène', 'C10H16'),
         ('acétate de 3,7-diméthylocta-1,6-dién-3-yle', 'C12H20O2.1'),
         ('(1S,4R)-1,3,3-Trimethylbicyclo[2.2.1]heptan-2-one', 'C10H16O.1'),
         ('1-isopropyl-4-méthylbicyclo[3.1.0]hexan-3-one', 'C10H16O.2'),
         ('propane-1,2,3-triol', 'C3H8O3'),
         ('Poly(dimethylsiloxane)', '(C2H6OSi)n'),
         ('RRR-α-tocophérol', 'C29H50O2')
         )

subActiv = (('Acide clavulanique', 'Substance issue du génie génétique', ['C8H9NO5'], 'D', ['Antalgique']),
            ('linalol', 'Synthèse chimique', ['C10H18O.0'], 'D', ['Antalgique', 'Antibactérienne']),
            ('1,8-cinéole', 'Extrait végétal', ['C10H18O.1'], 'D', ['Cardiotoniques',  'Antifongique']),
            ('camphre', 'Substance naturelle', ['C10H16O.0'], 'D', ['Anti-inflammatoire', 'Diurétiques',
                                                                   'Cicatrisante']),
            ('acétate de linalyle', 'Extrait minéral', ['C12H20O2.0'], 'D', ['Antiseptique',  'Neurotonique']),
            ('béta caryophyllène', 'Substance naturelle', ['C15H24'], 'D', ['Anticoagulant',  'Antispasmodique',
                                                                             'Antibactérienne']),
            ('terpinèn-4-ol', 'Substance issue du génie génétique', ['C10H18O.2'], 'D', ['Neurotonique']),
            ('cis beta ocimène', 'Synthèse hybride', ['C10H16'], 'D', ['Antiarythmiques']),
            ('acétate de lavandulyle', 'Synthèse chimique', ['C12H20O2.1'], 'D', ['Cicatrisante',  'Antiseptique']),
            ('Frenchone', 'Extrait végétal', ['C10H16O.1'], 'D', ['Décontractant musculaire']),
            ('Thuyone', 'Substance naturelle', ['C10H16O.2'], 'D', ['Hypotensive',  'Sédative']),
            ('Glycerines', 'Extrait minéral', ['C3H8O3'], 'D', ['Calmante',  'Antibactérienne']),
            ('Dimethicone', 'Substance naturelle', ['(C2H6OSi)n'], 'D', ['Carminative']),
            ('Tocophérol', 'Substance issue du génie génétique', ['C29H50O2'], 'D', ['Tonique cardiaque'])
            )

roles = ('Diluant', 'Conservateurs', 'Stabilisateur', 'Liant', 'Solubilisant', 'Délitants', 'Support - Forme',
         'Sapidité - Goût', 'Senteurs', 'Colorant', 'Ciblage', 'Emulsifiant', 'Gélifiant', 'Apparence')

effects = ('Allergies', 'Bouffée de chaleur', 'Dyspnée', 'Eczéma de contact', 'Hypersensibilité', 'Hypotension',
           'Inflammation', 'Insomnie', 'Intoxication', 'Irritabilité', 'Irritation', 'Neurotoxique',
           'Sécheresse cutanée', 'Trouble de la circulation')

exipCateg = ('Corps gras', 'Tensio-actifs', 'Conservateurs', 'Colorants', 'Parfum', 'Alcool', 'Solvant')

exipients = (('Maltodextrine', 'Support - Forme', ['Allergies', 'Irritabilité'], 'Tensio-actifs'),
             ('Aprotinine', 'Stabilisateur', ['Bouffée de chaleur', 'Irritation'], 'Conservateurs'),
             ('Colorants Azoïques', 'Apparence', ['Dyspnée', 'Neurotoxique'], 'Colorants'),
             ('Baume du Pérou', 'Emulsifiant', ['Eczéma de contact', 'Sécheresse cutanée'], 'Corps gras'),
             ('Chlorure de Benzalkonium', 'Ciblage', ['Hypersensibilité', 'Trouble de la circulation'], 'Colorants'),
             ('Acide Benzoïque et benzoates', 'Délitants', ['Hypotension', 'Intoxication'], 'Conservateurs'),
             ('Alcool Benzylique', 'Diluant', ['Inflammation', 'Insomnie', 'Sécheresse cutanée'], 'Alcool'),
             ('Huile de bergamote bergaptene', 'Conservateurs', ['Insomnie', 'Inflammation', 'Bouffée de chaleur'],
              'Corps gras'),
             ('alcools monoterpéniques', 'Solubilisant', ['Intoxication', 'Hypotension', 'Dyspnée'], 'Alcool'),
             ('Hydroxyanisole Butyle E320', 'Senteurs', ['Irritabilité', 'Allergies', 'Eczéma de contact'], 'Parfum'),
             ('Huile de ricin polyoxyl', 'Emulsifiant', ['Irritation', 'Bouffée de chaleur', 'Hypersensibilité'],
              'Corps gras'),
             ('Formaldehyde', 'Diluant', ['Neurotoxique', 'Dyspnée'], 'Alcool'),
             ('éthers', 'Sapidité - Goût', ['Sécheresse cutanée', 'Eczéma de contact'], 'Alcool'),
             ('éthanol', 'Support - Forme', ['Trouble de la circulation', 'Hypersensibilité'], 'Alcool'),
             ('chloroforme', 'Diluant', ['Irritabilité'], 'Solvant'),
             ('eau distillée', 'Solubilisant', ['Irritation', 'Hypotension'], 'Solvant'),
             ('eau osmosée', 'Diluant', ['Neurotoxique', 'Irritabilité', 'Allergies'], 'Solvant'),
             ('eau de source', 'Solubilisant', ['Sécheresse cutanée', 'Irritation'], 'Solvant'),
             ('fragrances', 'Senteurs', ['Trouble de la circulation', 'Neurotoxique'], 'Parfum'),
             ('Huile végétale abricot', 'Emulsifiant', ['Intoxication', 'Sécheresse cutanée',
                                                        'Trouble de la circulation'], 'Corps gras'),
             ('Huile végétale amande douce', 'Gélifiant', ['Insomnie', 'Trouble de la circulation', 'Intoxication'],
              'Corps gras'),
             ('Macérat huileux Arnica', 'Emulsifiant', ['Inflammation', 'Intoxication', 'Insomnie'], 'Corps gras'),
             ('Huile d\'argan', 'Gélifiant', ['Bouffée de chaleur', 'Insomnie', 'Inflammation'], 'Corps gras'),
             ('Glycerine', 'Emulsifiant', ['Dyspnée', 'Inflammation'], 'Corps gras'),
             ('Methylpropanediol', 'Diluant', ['Eczéma de contact', 'Hypotension', 'Irritabilité'], 'Solvant'),
             ('Steareth-2', 'Gélifiant', ['Hypersensibilité', 'Allergies', 'Irritation'], 'Tensio-actifs'),
             ('Myristyl Myristate', 'Emulsifiant', ['Hypotension', 'Bouffée de chaleur', 'Neurotoxique'],
              'Tensio-actifs'),
             ('Phenoxyethanol', 'Conservateurs', ['Inflammation', 'Dyspnée'], 'Conservateurs'),
             ('Acide sorbique', 'Conservateurs', ['Insomnie', 'Eczéma de contact'], 'Conservateurs'),
             ('Sodium benzoate', 'Ciblage', ['Intoxication', 'Hypersensibilité'], 'Conservateurs'),
             ('Potassium benzoate', 'Stabilisateur', ['Irritabilité'], 'Conservateurs'),
             ('Cire Abeille', 'Conservateurs', ['Irritation'], 'Corps gras'),
             ('Burre de karité', 'Stabilisateur', ['Neurotoxique'], 'Corps gras'),
             ('Coprah', 'Stabilisateur', ['Sécheresse cutanée'], 'Tensio-actifs'),
             ('Arachide', 'Emulsifiant', ['Trouble de la circulation'], 'Tensio-actifs')
             )

formul = (
    ('Huile essentielle Lavande', 'Lavande aspic en synthèse partielle', ['linalol', '1,8-cinéole', 'camphre']),
    ('Huile essentielle Lavande - naturelle', 'Lavande vrai bio en synthèse naturelle',
     ['linalol', 'acétate de linalyle', 'béta caryophyllène']),
    ('Hydratation plus', 'A diluer ou à utiliser avec précaution', ['terpinèn-4-ol', 'Frenchone']),
    ('Anti-âge - Révolution', 'En cours de recherche', ['cis beta ocimène', 'Glycerines', 'Dimethicone', 'Tocophérol'])
          )

#pdt_type = {'Sirop', 'Solution buvable', 'Suspension buvable', 'Pommades', 'Crêmes', 'Gel', 'Poudre', 'Solution',
#            'Poudre (lyophilisat)', 'Solution pour perfusion lente', 'Comprimé', 'Gelule', 'Orodispersible',
#            'Ovule', 'Capsule vaginale', 'Suppositoire', 'Aérosol'}

pdt_status = {'Recherche', 'Prototypage', 'Tests et validation', 'Pilote', 'DIP et CPNP', 'Industrialisation'}

uomCateg = ('Masse', 'Volume')

uom = (('g', 'gramme', 'Masse'), ('dg', 'décigramme', 'Masse'), ('cg', 'centigramme', 'Masse'),
       ('mg', 'milligramme', 'Masse'), ('µg', 'microgramme', 'Masse'), ('l', 'litre', 'Volume'),
       ('dl', 'décilitre', 'Volume'), ('cl', 'centilitre', 'Volume'), ('ml', 'millilitre', 'Volume'))

galenCateg = ('Solutions', 'Dispersions', 'Anhydres')

galen = (('Solution vraie', 'Mélange homogène où le soluté est en quantité minoritaire et le solvant en quantité'
                            ' majoritaire', 'Solutions'),
         ('Solution colloïdale', 'Dissolution de molécules de taille importante (= macromolécules) dans de l’eau.'
                                 ' Sa viscosité dépend de la concentration en soluté.', 'Solutions'),
         ('Gel', 'Il est constitué de macromolécules qui gonflent en présence du solvant, et forment un réseau '
                 'emprisonnant ce dernier.', 'Solutions'),
         ('Suspension', 'Dispersion de fines particules solides (poudres, pigments) dans un liquide. L’ajout de'
                        ' tensioactifs est souvent nécessaire pour stabiliser le produit.', 'Dispersions'),
         ('Emulsion', 'Dispersion d’un liquide dans un liquide (eau dans huile ou huile dans eau). Nécessite souvent'
                      ' la présence de Tensioactifs', 'Dispersions'),
         ('Aérosol', 'Dispersion de solide ou liquide dans un gaz. Le produit est propulsé tel une vaporisation en'
                     ' spray ', 'Dispersions'),
         ('Mousse', 'Dispersion de gaz liquéfié ou comprimé dans un liquide ou un solide. Aussi, le produit comprend un'
                    ' émulsionnant et un stabilisateur de mousse.', 'Dispersions'),
         ('Poudre', 'Fragmentation d’un solide en petites particules.', 'Anhydres'),
         ('Baume', 'Mélange de corps gras, plus ou moins épais et solide', 'Anhydres'),
         ('Huile', 'Seule ou combinées. Synthèse neutre ou support à une formule élaborée', 'Anhydres'))

gam = (
    ('Gamme n°B612 - Bêta 1', 'Crème main au miel', (
        ('1', ('Cire Abeille', 'eau de source', 'Tocophérol'), 'Préparation de la base'),
        ('2', ('Frenchone', 'Steareth-2', 'Formaldehyde'), 'Ajout des agents de texture'),
        ('3', (), 'Melange chauffé à 72.6°C pendant 38min'),
        ('4', (), 'Récupération des substrats'),
        ('5', ('Coprah', 'Arachide'), 'Stabilisation par mise au froid'),
    )),
    ('Gamme n°H3.18 - Alph 3', 'Huile pour le corps à la lavande', (
       ('1', ('linalol', '1,8-cinéole', 'camphre'), 'Mise en place du composé'),
       ('2', (), 'emulsion à haute fréquence pour hydrisation du composé'),
       ('3', ('eau osmosée', 'Huile végétale amande douce'), 'Mélange à la base'),
       ('4', (), 'Stabilisation par -5° pendant 2h'),
    )),
    ('Gamme n°K16L18', 'Recherche d\'un nouveau composé à inhibition lente', (
       ('1', ('Baume du Pérou', 'Acide clavulanique'), 'Tentative de saturation d\'un composé existant'),
       ('2', ('Formaldehyde', 'Chlorure de Benzalkonium', 'Aprotinine'), 'à compléter'),
   )),
   ('Gamme n°Proxima - Alpha', 'Stick pour les levres de l\'espace', (
       ('1', ('Acide clavulanique', 'Methylpropanediol', 'Baume du Pérou'), 'Entraînement de survie'),
       ('2', ('chloroforme', 'Alcool Benzylique'), 'Préparation sur Soyouz'),
       ('3', ('Macérat huileux Arnica', 'Aprotinine'), 'Décollage'),
       ('4', ('Chlorure de Benzalkonium', 'éthers'), 'Vacances en orbite'),
       ('5', ('Huile de ricin polyoxyl', 'Maltodextrine', 'Colorants Azoïques'), 'Désorbitation'),
       ('6', (), 'Tour d\'honneur et vulgarisation'),
       ('7', ('Glycerines', 'Aprotinine', 'linalol'), 'Et c\'est reparti pour un tour'),
       ('8', ('Huile végétale abricot', 'Colorants Azoïques', 'Baume du Pérou'),
        'Nouvelle mission, nouvelle navette, même hôtel'),
   )),
    ('Gamme n°EnSo1M', 'Réduction des actions androgènes de viellissements des parties molles', (
        ('1', ('Aprotinine', 'Maltodextrine'), 'Etape à valider'),
        ('2', ('Glycerine', 'Dimethicone'), 'Etape à valider'),
        ('3', ('Alcool Benzylique', 'Formaldehyde', 'fragrances', 'Colorants Azoïques'), 'Etape à valider'),
    ))
)

nome = (
    ('Liste des composants de B612 - Bêta 1', 'Crème main au miel ou presque', (
        ('Cire Abeille', '15', 'mg'),
        ('eau de source', '18', 'ml'),
        ('Tocophérol', '3', 'dg'),
        ('Frenchone', '8', 'ml'),
        ('Steareth-2', '4', 'cl'),
        ('Formaldehyde', '2', 'ml'),
        ('Coprah', '11', 'ml'),
        ('Arachide', '2', 'g')
    )),
    ('Liste des composants de H3.18 - Alph 3', 'Huile pour le corps à la lavande de synthèse', (
        ('linalol', '15', 'ml'),
        ('1,8-cinéole', '3', 'g'),
        ('camphre', '2', 'mg'),
        ('eau osmosée', '17', 'ml'),
        ('Huile végétale amande douce', '8', 'dl')
    )),
    ('Liste des composants de n°K16L18', 'Recherche d\'un nouveau composé à inhibition lente', (
        ('Baume du Pérou', '13', 'g'),
        ('Acide clavulanique', '22', 'dl'),
        ('Formaldehyde', '35', 'cl'),
        ('Chlorure de Benzalkonium', '85', 'ml'),
        ('Aprotinine', '25', 'dg')
    )),
    ('Liste des composants de Proxima - Alpha', 'Stick pour les levres de l\'espace', (
        ('Acide clavulanique', '3', 'ml'),
        ('Methylpropanediol', '38', 'ml'),
        ('Baume du Pérou', '7', 'dg'),
        ('Macérat huileux Arnica', '3', 'mg'),
        ('Aprotinine', '12', 'mg'),
        ('Chlorure de Benzalkonium', '2', 'dg'),
        ('éthers', '18', 'ml'),
        ('Huile de ricin polyoxyl', '3', 'dl'),
        ('Maltodextrine', '2', 'g'),
        ('Colorants Azoïques', '2', 'ml'),
        ('Glycerines', '33', 'mg'),
        ('linalol', '9', 'ml'),
        ('Huile végétale abricot', '9', 'ml')
        )),
    ('Liste des composants de EnSo1M', 'Réduction des actions androgènes de viellissements des parties molles', (
        ('Aprotinine', '18', 'mg'),
        ('Maltodextrine', '1123', 'dg'),
        ('Glycerine', '1', 'g'),
        ('Dimethicone', '35', 'mg'),
        ('Alcool Benzylique', '12', 'cl'),
        ('Formaldehyde', '56', 'cl'),
        ('fragrances', '133', 'ml'),
        ('Colorants Azoïques', '3', 'ml')
    ))
)

pots = (
    ('Flacon verre multiusage', ('Solution vraie', 'Solution colloïdale', 'Suspension', 'Huile'),
     'Peut accepter différents types de produits et de bouchons (classiques, goutte à goutte, ...)',
     'Cassant. Faible volume', 'Aroma.png', 'cbs'),
    ('Flacon plastique bouchon bascule', ('Gel', 'Emulsion', 'Huile'),
     'Dosage précis, hygiénique et rechargeable. Bonne résistance.',
     'Petit volume. Produit de grande consommation', 'flacon.png', 'cbs'),
    ('Flacon plastique pompe', ('Solution vraie', 'Solution colloïdale', 'Gel', 'Emulsion', 'Huile'),
     'Dosage très précis, hygiénique et rechargeable.',
     'Pompe à faible durée de vie', 'flacon_pompe.png', 'cbs'),
    ('Flacon verre pompe opaque', ('Solution vraie', 'Solution colloïdale', 'Gel', 'Emulsion', 'Huile'),
     'Grande capacité. Dosage précis, hygyiniue et rechargeable. Bonne conservation',
     'Cassant. Volume important. Pompe encrassable si galénique trop dense', 'flacon_pompe_opaque.jfif', 'cbs'),
    ('Flacon plastique spray', ('Solution vraie', 'Solution colloïdale', 'Huile'),
     'Pulvérisateur large spectre. Flacon plastique rechargeable',
     'Capuchon fragile. Faible durée de vie.', 'flacon_spray.png', 'cbs'),
    ('Goutte à goutte', ('Solution vraie', 'Solution colloïdale', 'Huile'),
     'Produit au finition luxe avec couvercle en bois. Pipette précise',
     'Durée de vie de la pipette à améliorer. Verre présentant des aspérités.', 'Goutte.jfif', 'cbs'),
    ('Pot aluminium', ('Gel', 'Suspension', 'Emulsion', 'Poudre', 'Baume'),
     'Très bonne conservation. Usage et stockage Ok',
     'Galénique à tendance grace ou sèche à privilégier.', 'Pot_alu_bis.jpg', 'cbs'),
    ('Pot verre', ('Gel', 'Suspension', 'Emulsion', 'Poudre', 'Baume'), 'Produit luxe. Verre de bonne qualité.',
     'Faible durée de conservation après ouverture. Cassant.', 'pot_verre.jpg', 'cbs'),
    ('RollOn', ('Solution vraie', 'Solution colloïdale', 'Huile'),
     'Peu d\'échanges avec l\'air donc conservation longue durée',
     'Galénique liquide à privilégier. Usage individuel. Déchêts importants', 'Roll_On_Odda.png', 'cbs'),
    ('Tube', ('Gel', 'Suspension', 'Emulsion'),
     'Idéal pour crème et gel en mode nomade. Plusieurs volumes disponibles.',
     'Perte de produit et déchêts importants (non rechargeable)', 'tube.png', 'cbs')
       )

pdt = (
    (
        'Lavandou', 'V1.2', 3, 'g', 'Huile', 6, 'Prototypage', 'Huile essentielle Lavande - naturelle',
        'Liste des composants de H3.18 - Alph 3', 'Gamme n°H3.18 - Alph 3', 'Essai n°6 Formule 14'
     ),
    (
        'ForEverYoung', 'V0.3', 250, 'ml', 'Baume', 10, 'Pilote', 'Anti-âge - Révolution',
        'Liste des composants de n°K16L18', 'Gamme n°K16L18', 'Essai n°4 Formule 33'
    ),
    (
        'MainBelle', 'V0.3', 250, 'ml', 'Gel', 10, 'DIP et CPNP', 'Hydratation plus',
        'Liste des composants de B612 - Bêta 1', 'Gamme n°B612 - Bêta 1', 'Essai n°4 Formule 33'
    )
)


class Command(BaseCommand):
    help = 'Initialise la base de données et injecte le jeu de test principal'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):

        a = User.objects.all()
        if len(a) > 0:
            a.delete()
        for e in users:
            use = User.objects.create(username=e[0], email=e[0] + "@buenavista.com")
            use.set_password(e[1])
            use.save()
        print("users ok")

        a = Visibility.objects.all()
        if len(a) > 0:
            a.delete()
        for e in visibility:
            Visibility.objects.create(title=e)
        print("Visibility ok")

        a = Document.objects.all()
        if len(a) > 0:
            a.delete()
        for e in docs:
            doc = Document.objects.create(author=User.objects.get(username=e[0]),
                                          title=e[1],
                                          type=e[2],
                                          visibility=Visibility.objects.get(title=e[3]))
            doc.piece.save(e[4], File(open(settings.MEDIA_ROOT + '/tmp/' + e[4], 'rb')))
        print("Documents ok")

        a = Origin.objects.all()
        if len(a) > 0:
            a.delete()
        for e in origin:
            Origin.objects.create(title=e)
        print("Origin ok")

        a = Aim.objects.all()
        if len(a) > 0:
            a.delete()
        for e in aim:
            Aim.objects.create(title=e)
        print("Aim Ok")

        a = Molecule.objects.all()
        if len(a) > 0:
            a.delete()
        for e in moles:
            Molecule.objects.create(name=e[0], symbol=e[1])
        print("Molecule Ok")

        a = ActiveSubstance.objects.all()
        if len(a) > 0:
            a.delete()
        for e in subActiv:
            actSub = ActiveSubstance.objects.create(title=e[0], origin=Origin.objects.get(title=e[1]))
            for elt in e[2]:
                actSub.molecule.add(Molecule.objects.get(symbol=elt))
            for elt in e[4]:
                actSub.aim.add(Aim.objects.get(title=elt))
            actSub.save()
        print("SubstanceActive Ok")

        a = Role.objects.all()
        if len(a) > 0:
            a.delete()
        for e in roles:
            Role.objects.create(title=e)
        print("Roles Ok")

        a = Effect.objects.all()
        if len(a) > 0:
            a.delete()
        for e in effects:
            Effect.objects.create(title=e)
        print("Effect Ok")

        a = ExcipientCategory.objects.all()
        if len(a) > 0:
            a.delete()
        for e in exipCateg:
            ExcipientCategory.objects.create(name=e)
        print("ExcipientCategory Ok")

        a = Excipient.objects.all()
        if len(a) > 0:
            a.delete()
        for e in exipients:
            exc = Excipient.objects.create(title=e[0],
                                           role=Role.objects.get(title=e[1]),
                                           category=ExcipientCategory.objects.get(name=e[3]))
            for elt in e[2]:
                exc.effects.add(Effect.objects.get(title=elt))
            exc.save()
        print("Excipient Ok")

        a = Formula.objects.all()
        if len(a) > 0:
            a.delete()
        for e in formul:
            f = Formula.objects.create(title=e[0], memo=e[1])
            for elt in e[2]:
                f.composition.add(ActiveSubstance.objects.get(title=elt))
            f.save()
        print("Formula Ok")

        a = Scale.objects.all()
        if len(a) > 0:
            a.delete()
        for e in [1, 2, 3]:
            Scale.objects.create(title="Gamme n°B612 - Bêta " + str(e))
        print("Scale Ok")

        a = Nomenclature.objects.all()
        if len(a) > 0:
            a.delete()
        for e in [1, 2, 3, 4]:
            Nomenclature.objects.create(title="Liste des composants de l'essai n°42-1" + str(e))
        print("Nomenclature Ok")

        a = Status.objects.all()
        if len(a) > 0:
            a.delete()
        for e in pdt_status:
            Status.objects.create(name=e)
        print("Status Ok")

        a = UOMCategory.objects.all()
        if len(a) > 0:
            a.delete()
        for e in uomCateg:
            UOMCategory.objects.create(name=e)
        print("UOMCategory Ok")

        a = UnitOfMeasure.objects.all()
        if len(a) > 0:
            a.delete()
        for e in uom:
            UnitOfMeasure.objects.create(name=e[1], symbol=e[0], category=UOMCategory.objects.get(name=e[2]))
        print("UnitOfMeasure Ok")

        a = GalenicCategory.objects.all()
        if len(a) > 0:
            a.delete()
        for e in galenCateg:
            GalenicCategory.objects.create(name=e)
        print("GalenicCategory Ok")

        a = Galenic.objects.all()
        if len(a) > 0:
            a.delete()
        for e in galen:
            Galenic.objects.create(name=e[0], details=e[1], category=GalenicCategory.objects.get(name=e[2]))
        print("Galenic Ok")

        a = Scale.objects.all()
        if len(a) > 0:
            a.delete()
        for e in gam:
            s = Scale.objects.create(title=e[0], description=e[1])
            for elt in e[2]:
                sd = ScaleDetail.objects.create(scale=s, num=elt[0], step=elt[2])
                for lol in elt[1]:
                    sd.item.add(Element.objects.get(title=lol))
                sd.save()
        print("Scale Ok")


        a = Nomenclature.objects.all()
        if len(a) > 0:
            a.delete()
        for e in nome:
            n = Nomenclature.objects.create(title=e[0], description=e[1])
            for elt in e[2]:
                NomenclatureDetail.objects.create(nomenclature=n, item=Element.objects.get(title=elt[0]),
                                                  quantity=elt[1], unit=UnitOfMeasure.objects.get(symbol=elt[2]))
        print("Nomenclature Ok")

        a = Container.objects.all()
        if len(a) > 0:
            a.delete()
        for e in pots:
            pot = Container.objects.create(author=User.objects.get(username=e[5]),
                                           title=e[0], advantages=e[2], improves=e[3])
            for elt in e[1]:
                pot.galenic.add(Galenic.objects.get(name=elt))
            pot.save()
            pot.design.save(e[4], File(open(settings.MEDIA_ROOT + '/tmp/pots/' + e[4], 'rb')))
        print("Container ok")

        a = Product.objects.all()
        if len(a) > 0:
            a.delete()
        for e in pdt:
            p = Product.objects.create(name=e[0],
                                       version=e[1],
                                       volume=e[2],
                                       unit_of_measure=UnitOfMeasure.objects.get(symbol=e[3]),
                                       galenic=Galenic.objects.get(name=e[4]),
                                       expiration=e[5],
                                       status=Status.objects.get(name=e[6]),
                                       formula=Formula.objects.get(title=e[7]),
                                       nomenclature=Nomenclature.objects.get(title=e[8]),
                                       scale=Scale.objects.get(title=e[9]),
                                       document=Document.objects.get(title=e[10]))
        print("Produit Ok")
