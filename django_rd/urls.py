"""django_rd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rd.views import main


urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('accounts/api/', include('accounts.urls_api')),
    path('product/', include('product.urls')),
    path('product/api/', include('product.urls_api')),
    path('address/', include('rd.urls')),
    path('address/api/', include('rd.urls_api')),
    path('ged/', include('ged.urls')),
    path('ged/api/', include('ged.urls_api')),
    path('element/', include('element.urls')),
    path('element/api/', include('element.urls_api')),
    path('research/', include('research.urls')),
    path('research/api/', include('research.urls_api')),
    path('', main, name='index'),
]
