from utils.BaseView import ERPView
from product.models import Status
from product.serializers import StatusGETSerializer, StatusPOSTSerializer


class StatusView(ERPView):

    model = Status
    post_serializer = StatusPOSTSerializer
    get_serializer = StatusGETSerializer
