from utils.BaseView import ERPView
from product.models import *
from research.models import *
from element.models import *
from product.serializers import ProductGETSerializer, ProductPOSTSerializer
from rest_framework import status
from rest_framework.response import Response
from django.conf import settings


class ProductView(ERPView):

    model = Product
    post_serializer = ProductPOSTSerializer
    get_serializer = ProductGETSerializer

    def put(self, request, *args, **kwargs):
        response = super(ProductView, self).put(request, *args, **kwargs)
        data = response.data

        mat_prem = []
        mat_prem_id = []

        uid = Product.objects.get(name=data.get('name')).id

        statut = Status.objects.get(pk=data.get('status'))

        galenic = Galenic.objects.get(pk=data.get('galenic'))
        formula = Formula.objects.get(pk=data.get('formula'))
        nomenclature = Nomenclature.objects.get(pk=data.get('nomenclature'))
        scale = Scale.objects.get(pk=data.get('scale'))
        unite_de_mesure = UnitOfMeasure.objects.get(pk=data.get('unit_of_measure'))

        unit = []
        unit_id = [unite_de_mesure.id]

        unit.append({
            'nom': unite_de_mesure.name,
            'symbole': unite_de_mesure.symbol,
            'categorie': unite_de_mesure.category.name
        })

        unit.append({
            'nom': 'milligramme',
            'symbole': 'mg',
            'categorie': 'Masse'
        })

        nomenclature_detail = []
        for e in nomenclature.nomenclaturedetail_set.all():
            nomenclature_detail.append({
                'Matiere-Premiere': {
                    'Nom': e.item.title,
                    'Code': e.item.id
                },
                'Quantite': e.quantity,
                'Unite': e.unit.symbol
            })

            if e.unit.id not in unit_id:
                unit_id.append(e.unit.id)
                unit.append({
                    'nom': e.unit.name,
                    'symbole': e.unit.symbol,
                    'categorie': e.unit.category.name
                })

            if e.item.id not in mat_prem_id:
                mat_prem_id.append(e.item.id)
                if isinstance(e.item, ActiveSubstance):
                    typo = 'Substance Active'
                elif isinstance(e.item, Excipient):
                    typo = 'Excipient'
                else:
                    typo = 'Composé chimique'
                mat_prem.append({
                    'nom': e.item.title,
                    'unite_de_mesure': e.unit.symbol,
                    'type': typo
                })

        scale_detail = []
        for e in scale.scaledetail_set.all():
            items = []
            for m in e.item.all():
                items.append(m.title)

                if m.id not in mat_prem_id:
                    mat_prem_id.append(m.id)
                    if isinstance(m, ActiveSubstance):
                        typo = 'Substance Active'
                    elif isinstance(m, Excipient):
                        typo = 'Excipient'
                    else:
                        typo = 'Composé chimique'
                    mat_prem.append({
                        'nom': m.title,
                        'unite_de_mesure': 'mg',
                        'type': typo
                    })

            scale_detail.append({
                'order': e.num,
                'item': items,
                'step': e.step
            })

        composition = []
        for e in formula.composition.all():
            composition.append(e.title)
            if e.id not in mat_prem_id:
                mat_prem_id.append(e.id)
                if isinstance(e, ActiveSubstance):
                    typo = 'Substance Active'
                elif isinstance(e, Excipient):
                    typo = 'Excipient'
                else:
                    typo = 'Composé chimique'
                mat_prem.append({
                    'nom': e.title,
                    'unite_de_mesure': 'mg',
                    'type': typo
                })

        galenCateg=str(galenic.category)
        if galenCateg == 'Solutions':
            category = 'Creme'
        elif galenCateg == 'Dispersions':
            category = 'Lotion'
        elif galenCateg == 'Anhydres':
            category = 'Cachet'
        else:
            category = 'Parfum'

        if statut.name == 'Industrialisation':
            data.update({
                'esb': {
                    'url': settings.ESB_ADDRESS,
                    'produit': {
                        'Nom': data.get('name'),
                        'Code': uid,
                        'category': category,
                        'volume': data.get('volume'),
                        'unite_de_mesure': unite_de_mesure.symbol,
                        'galenic': galenic.name,
                        'expiration': data.get('expiration'),
                        'formule': formula.title,
                        'nomenclature': nomenclature.title,
                        'gamme': scale.title,
                        'Recette': nomenclature_detail
                    }
                }
            })
        return Response(data, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        pk_product = kwargs.get('pk', None)

        try:
            product = Product.objects.get(pk=pk_product)
        except Product.DoesNotExist:
            return Response({'message': 'The product does not exist'}, status=status.HTTP_404_NOT_FOUND)

        memo = ""
        if product.volume is not None and product.unit_of_measure is not None:
            memo += "Volume" + str(product.volume) + " " + str(product.unit_of_measure) + " / "

        if product.formula is not None:
            memo += "Formula : " + str(product.formula) + " / "

        if product.expiration is not None:
            memo += "Expiration : " + str(product.expiration) + " / "

        if product.nomenclature is not None:
            memo += "Nomenclature : " + str(product.nomenclature) + " / "

        if product.scale is not None:
            memo += "Scale : " + str(product.scale) + " / "

        if product.document is not None:
            memo += "Document : " + str(product.document) + " / "

        if len(memo) > 2:
            memo = memo[:len(memo)-2]

        History.objects.create(name=product.name, version=product.version, status=product.status,
                               deleted_by=request.user, memo=memo)

        product.delete()

        return Response({'message': 'The product was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
