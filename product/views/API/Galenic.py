from utils.BaseView import ERPView
from product.models import Galenic
from product.serializers import GalenicPOSTSerializer, GalenicGETSerializer


class GalenicView(ERPView):

    model = Galenic
    post_serializer = GalenicPOSTSerializer
    get_serializer = GalenicGETSerializer
