from utils.BaseView import ERPView
from product.models import History
from product.serializers import HistoryGETSerializer, HistoryPOSTSerializer


class HistoryView(ERPView):

    model = History
    post_serializer = HistoryPOSTSerializer
    get_serializer = HistoryGETSerializer
