from utils import get_default_data
from product.forms.UnitOfMeasure import UnitOfMeasureForm
from django.http import JsonResponse, HttpResponseForbidden


def unit_of_measure_index(request):
    if request.is_ajax():
        columns = ["", "Name", "Symbol", "Category"]
        data = get_default_data(request, "unit-of-measure", UnitOfMeasureForm, None, columns,
                                "product/api/unit-of-measure/")
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'name'},
            {'data': 'symbol'},
            {'data': 'category.name'},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
