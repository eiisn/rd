from utils import get_default_data
from product.forms.Status import StatusForm
from django.http import JsonResponse, HttpResponseForbidden


def status_index(request):
    if request.is_ajax():
        columns = ["", "Name"]
        data = get_default_data(request, "status", StatusForm, None, columns, "product/api/status/",
                                title='Unit of measure')
        data["options"]["columns"] = data["options"]["columns"] + [{
            "data": "name"
        }]
        return JsonResponse(data)
    return HttpResponseForbidden()
