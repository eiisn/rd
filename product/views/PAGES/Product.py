from utils import get_default_data
from product.forms.Product import ProductForm
from django.http import JsonResponse, HttpResponseForbidden


def product_index(request):
    if request.is_ajax():
        columns = ["", "Name", "Version", "Volume", "Unit of measure", "Galenic", "Expiration", "Status", "Formula",
                   "Nomenclature", "Scale", "Document"]
        data = get_default_data(request, "product", ProductForm, None, columns, "product/api/product/", 1, [7, 8, 9],
                                title='Products')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "name"},
            {"data": "version"},
            {"data": "volume", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "unit_of_measure.name", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "galenic.name", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "expiration"},
            {"data": "status.name"},
            {"data": "formula.title", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "nomenclature.title", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "scale.title", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"},
            {"data": "document.title", "defaultContent": "<i class=\"fas fa-exclamation-triangle\"></i>"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
