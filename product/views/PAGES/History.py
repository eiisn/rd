from utils import get_default_data
from product.forms.History import HistoryForm
from django.http import JsonResponse, HttpResponseForbidden


def history_index(request):
    if request.is_ajax():
        columns = ["", "Name", "Version", "Status", "Delete date", "By", "Memo"]
        data = get_default_data(request, "history", HistoryForm, None, columns,
                                "product/api/history/")
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'name'},
            {'data': 'version'},
            {'data': 'status'},
            {'data': 'deleted_date'},
            {'data': 'deleted_by'},
            {'data': 'memo'}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
