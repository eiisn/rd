from utils import get_default_data
from product.forms.Galenic import GalenicForm
from django.http import JsonResponse, HttpResponseForbidden


def galenic_index(request):
    if request.is_ajax():
        columns = ["", "Name", "Details", "Category"]
        data = get_default_data(request, "galenic", GalenicForm, None, columns,
                                "product/api/galenic/", title='Galenics')
        data["options"]["columns"] = data["options"]["columns"] + [
            {'data': 'name'},
            {'data': 'details'},
            {'data': 'category.name'},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
