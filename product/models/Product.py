from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=255)
    volume = models.IntegerField(null=True)
    version = models.CharField(max_length=25, default='VO.O')
    expiration = models.IntegerField(default=6)
    nomenclature = models.ForeignKey("research.Nomenclature", models.SET_NULL, null=True)
    scale = models.ForeignKey("research.Scale", models.SET_NULL, null=True)
    galenic = models.ForeignKey("product.Galenic", models.SET_NULL, null=True)
    unit_of_measure = models.ForeignKey("product.UnitOfMeasure", models.SET_NULL, null=True)
    formula = models.ForeignKey("research.Formula", models.SET_NULL, null=True)
    status = models.ForeignKey("product.Status", models.SET_NULL, null=True)
    document = models.ForeignKey("ged.Document", models.SET_NULL, null=True)

    def __str__(self):
        return "%s (%s)" % (self.name, self.version)
