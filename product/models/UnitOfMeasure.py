from django.db import models


class UnitOfMeasure(models.Model):
    name = models.CharField(max_length=100)
    symbol = models.CharField(max_length=50)
    category = models.ForeignKey("UOMCategory", models.SET_NULL, 'uom_category', null=True)

    def __str__(self):
        return "%s (%s)" % (self.name, self.symbol)


class UOMCategory(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Unit of measure category"
        verbose_name_plural = "Units of measures categories"

    def __str__(self):
        return self.name
