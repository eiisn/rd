from django.db import models


class Galenic(models.Model):
    name = models.CharField(max_length=100)
    details = models.CharField(max_length=250)
    category = models.ForeignKey("GalenicCategory", models.SET_NULL, 'galenic_category', null=True)

    def __str__(self):
        return self.name


class GalenicCategory(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "Galenic category"
        verbose_name_plural = "Galenics categories"

    def __str__(self):
        return self.name
