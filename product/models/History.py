from django.db import models


class History(models.Model):
    name = models.CharField(max_length=255)
    version = models.CharField(max_length=25)
    status = models.CharField(max_length=100)
    deleted_date = models.DateField(auto_now_add=True)
    deleted_by = models.CharField(max_length=100)
    memo = models.CharField(max_length=500)

    def __str__(self):
        return self.name
