"""product URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from product.views.PAGES import *

urlpatterns = [
    path('get-product-index/', product_index, name="product-index"),
    path('get-unit-of-measure-index/', unit_of_measure_index, name='unit-of-measure-index'),
    path('get-history-index/', history_index, name='history-index'),
    path('get-status-index/', status_index, name='status-index'),
    path('get-galenic-index/', galenic_index, name='galenic-index'),
]
