from django import forms
from product.models import History


class HistoryForm(forms.ModelForm):

    class Meta:
        model = History
        exclude = ('id',)
