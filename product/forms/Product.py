from django import forms
from product.models import Product
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Field


class ProductForm(forms.ModelForm):

    class Meta:
        model = Product
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['galenic'].required = False
        self.fields['volume'].required = False
        self.fields['unit_of_measure'].required = False
        self.fields['document'].required = False
        self.fields['formula'].required = False
        self.fields['scale'].required = False
        self.fields['nomenclature'].required = False
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('name'),
            Field('volume'),
            Field('version'),
            Field('expiration'),
            Field('nomenclature'),
            Field('scale'),
            Field('galenic'),
            Field('unit_of_measure'),
            Field('formula'),
            Field('status'),
            Field('document'),
        )
