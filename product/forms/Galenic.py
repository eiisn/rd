from django import forms
from product.models import Galenic
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div, Button


class GalenicForm(forms.ModelForm):
    new_category = forms.CharField(max_length=64, required=False)

    class Meta:
        model = Galenic
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        super(GalenicForm, self).__init__(*args, **kwargs)
        self.fields['category'].required = False
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('name'),
            Field('details'),
            Div(
                Div(Field('category'), css_class="col"),
                Div(Button("Ou", "Ou", disabled="ok"), css_class="col"),
                Div(Field('new_category'), css_class="col"),
                css_class="row"
            ),
        )
