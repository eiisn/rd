"""product URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from product.views.API import *

urlpatterns = [
    path('product/', ProductView.as_view()),
    path('product/<int:pk>/', ProductView.as_view()),
    path('unit-of-measure/', UnitOfMeasureView.as_view()),
    path('unit-of-measure/<int:pk>/', UnitOfMeasureView.as_view()),
    path('history/', HistoryView.as_view()),
    path('history/<int:pk>/', HistoryView.as_view()),
    path('status/', StatusView.as_view()),
    path('status/<int:pk>/', StatusView.as_view()),
    path('galenic/', GalenicView.as_view()),
    path('galenic/<int:pk>/', GalenicView.as_view())
]
