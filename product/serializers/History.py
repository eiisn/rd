from rest_framework import serializers
from product.models import History


class HistoryGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = History
        fields = '__all__'


class HistoryPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = History
        exclude = ['id']
