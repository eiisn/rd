from rest_framework import serializers
from product.models import Galenic, GalenicCategory


class GalenicGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Galenic
        fields = '__all__'
        depth = 1


class GalenicPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Galenic
        fields = '__all__'

    def create(self, validated_data):
        new_category = validated_data.pop('new_category', None)
        if new_category and not validated_data["category"]:
            validated_data['category'] = GalenicCategory.objects.get_or_create(name=new_category)[0]
        return Galenic.objects.create(**validated_data)

    def to_internal_value(self, data):
        internal_value = super(GalenicPOSTSerializer, self).to_internal_value(data)
        new_category = data.get("new_category")
        if new_category:
            internal_value.update({
                "new_category": new_category
            })
        return internal_value
