from rest_framework import serializers
from product.models import Status


class StatusGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Status
        fields = '__all__'


class StatusPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Status
        exclude = ['id']
