from rest_framework import serializers
from product.models import UnitOfMeasure, UOMCategory


class UnitOfMeasureGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = UnitOfMeasure
        fields = '__all__'
        depth = 1


class UnitOfMeasurePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = UnitOfMeasure
        fields = '__all__'

    def create(self, validated_data):
        new_category = validated_data.pop('new_category', None)
        if new_category and not validated_data["category"]:
            validated_data['category'] = UOMCategory.objects.get_or_create(name=new_category)[0]
        return UnitOfMeasure.objects.create(**validated_data)

    def to_internal_value(self, data):
        internal_value = super(UnitOfMeasurePOSTSerializer, self).to_internal_value(data)
        new_category = data.get("new_category")
        if new_category:
            internal_value.update({
                "new_category": new_category
            })
        return internal_value
