from rest_framework import serializers
from product.models import Product


class ProductGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'
        depth = 2


class ProductPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        exclude = ['id']
