from django.contrib import admin
from product.models import *

# Register your models here.
admin.site.register(Product)
admin.site.register(UOMCategory)
admin.site.register(UnitOfMeasure)
admin.site.register(Status)
admin.site.register(History)
admin.site.register(Galenic)
admin.site.register(GalenicCategory)

