"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from research.views.PAGES import *


urlpatterns = [
    path('get-formula-index/', formula_index, name='formula-index'),
    path('get-scale-index/', scale_index, name='scale-index'),
    path('get-scale-detail-index/<int:pk_scale>/', scale_detail_index, name="scale-detail-index"),
    path('get-container-index/', container_index, name='container-index'),
    path('get-nomenclature-index/', nomenclature_index, name='nomenclature-index'),
    path('get-nomenclature-detail-index/<int:pk_nomenclature>/', nomenclature_detail_index,
         name="nomenclature-detail-index")

]
