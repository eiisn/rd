from django.db import models


class Formula(models.Model):
    title = models.CharField(max_length=100)
    memo = models.CharField(max_length=500)
    composition = models.ManyToManyField("element.ActiveSubstance")

    def __str__(self):
        return self.title
