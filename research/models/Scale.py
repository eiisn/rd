from django.db import models


class Scale(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.title


class ScaleDetail (models.Model):
    scale = models.ForeignKey(Scale, models.CASCADE, null=True)
    num = models.IntegerField()
    item = models.ManyToManyField("element.Element", blank=True)
    step = models.CharField(max_length=500)

    def __str__(self):
        return "%s - %s" % (
            self.scale,
            self.step[:50]
        )
