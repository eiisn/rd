from django.db import models
from django.conf import settings
from utils.media_path import user_directory_path


class Container(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=100)
    galenic = models.ManyToManyField("product.Galenic", blank=True)
    advantages = models.CharField(max_length=500)
    improves = models.CharField(max_length=500)
    design = models.FileField(upload_to=user_directory_path, null=True, blank=True)

    def __str__(self):
        return self.title
