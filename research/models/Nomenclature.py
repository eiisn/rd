from django.db import models


class Nomenclature(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.title


class NomenclatureDetail (models.Model):
    nomenclature = models.ForeignKey(Nomenclature, models.CASCADE, null=True)
    item = models.ForeignKey("element.Element", models.SET_NULL, null=True)
    quantity = models.IntegerField()
    unit = models.ForeignKey("product.UnitOfMeasure", models.SET_NULL, null=True)

    def __str__(self):
        return "%s %s x %s" % (
            self.quantity,
            self.unit.symbol,
            self.item.title,
        )
