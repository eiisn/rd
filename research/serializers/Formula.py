from rest_framework import serializers
from research.models import Formula
from element.models import ActiveSubstance


class FormulaGETSerializer(serializers.ModelSerializer):

    composition = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='title'
     )

    class Meta:
        model = Formula
        fields = '__all__'
        depth = 2


class FormulaPOSTSerializer(serializers.ModelSerializer):

    composition = serializers.SlugRelatedField(
        many=True,
        queryset=ActiveSubstance.objects.all(),
        slug_field='id'
     )

    class Meta:
        model = Formula
        exclude = ['id']

