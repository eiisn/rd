from rest_framework import serializers
from research.models import Container
from product.models import Galenic


class ContainerGETSerializer(serializers.ModelSerializer):

    galenic = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='name'
    )

    class Meta:
        model = Container
        fields = '__all__'
        depth = 2


class ContainerPOSTSerializer(serializers.ModelSerializer):

    galenic = serializers.SlugRelatedField(
        many=True,
        queryset=Galenic.objects.all(),
        slug_field='id'
    )

    class Meta:
        model = Container
        exclude = ['id']
