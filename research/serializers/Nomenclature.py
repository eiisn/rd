from rest_framework import serializers
from research.models import Nomenclature, NomenclatureDetail


class NomenclatureGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Nomenclature
        fields = '__all__'
        depth = 2


class NomenclaturePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Nomenclature
        exclude = ['id']


class NomenclatureDetailGETSerializer(serializers.ModelSerializer):
    class Meta:
        model = NomenclatureDetail
        fields = '__all__'
        depth = 2


class NomenclatureDetailPOSTSerializer(serializers.ModelSerializer):
    class Meta:
        model = NomenclatureDetail
        exclude = ['id']

    def __init__(self, nomenclature=None, *args, **kwargs):
        super(NomenclatureDetailPOSTSerializer, self).__init__(*args, **kwargs)
        self.__nomenclature = nomenclature

    def create(self, validated_data):
        validated_data['nomenclature'] = Nomenclature.objects.get(pk=self.__nomenclature)
        return NomenclatureDetail.objects.create(**validated_data)
