from rest_framework import serializers
from research.models import Scale, ScaleDetail
from element.models import Element

class ScaleGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Scale
        fields = '__all__'
        depth = 2


class ScalePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Scale
        exclude = ['id']


class ScaleDetailGETSerializer(serializers.ModelSerializer):

    item = serializers.SlugRelatedField(
        many=True,
        queryset=Element.objects.all(),
        slug_field='title'
    )

    class Meta:
        model = ScaleDetail
        fields = '__all__'
        depth = 2


class ScaleDetailPOSTSerializer(serializers.ModelSerializer):

    item = serializers.SlugRelatedField(
        many=True,
        queryset=Element.objects.all(),
        slug_field='id'
    )

    class Meta:
        model = ScaleDetail
        exclude = ['id']

    def __init__(self, scale=None, *args, **kwargs):
        super(ScaleDetailPOSTSerializer, self).__init__(*args, **kwargs)
        self.__scale = scale

    def create(self, validated_data):
        validated_data['scale'] = Scale.objects.get(pk=self.__scale)
        items = validated_data['item']
        del validated_data['item']
        instance = ScaleDetail.objects.create(**validated_data)
        for e in items:
            instance.item.add(Element.objects.get(title=e))
        instance.save()
        return instance
