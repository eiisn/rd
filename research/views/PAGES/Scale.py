from research.forms import ScaleForm, ScaleDetailForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden
from research.models import Scale


def scale_index(request):
    if request.is_ajax():
        columns = ["", "", "Title", "Description"]
        data = get_default_data(request, "scale", ScaleForm,
                                None, columns, "research/api/scale/", 2, script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {"data": "title"},
            {"data": "description"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def scale_detail_index(request, pk_scale):
    z = Scale.objects.get(pk=pk_scale)
    if request.is_ajax():
        columns = ["", "Order", "Item", "Step"]
        data = get_default_data(request, "scale-detail-%s" % pk_scale, ScaleDetailForm,
                                None, columns, "research/api/scale-detail/%s/" % pk_scale, 1, [2],
                                initial_form_add={"scale": pk_scale})
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "num"},
            {"data": "item"},
            {"data": "step"}
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
