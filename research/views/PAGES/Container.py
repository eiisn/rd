from research.forms import ContainerForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def container_index(request):
    if request.is_ajax():
        columns = ["", "Title", "Galenic", "Advantages", "Improves", "Design"]
        data = get_default_data(request, "container", ContainerForm, None, columns, "research/api/container/", 1,
                                [2, 5], title='Containers')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
            {"data": "galenic"},
            {"data": "advantages"},
            {"data": "improves"},
            {"data": "design"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
