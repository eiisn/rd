from research.forms import NomenclatureForm, NomenclatureDetailForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def nomenclature_index(request):
    if request.is_ajax():
        columns = ["", "", "Title", "Description"]
        data = get_default_data(request, "nomenclature", NomenclatureForm,
                                None, columns, "research/api/nomenclature/", 2, script=True)
        data["options"]["columns"] = data["options"]["columns"] + [
            {
                "className": "details-control",
                "orderable": False,
                "data": None,
                "defaultContent": ''
            },
            {"data": "title"},
            {"data": "description"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def nomenclature_detail_index(request, pk_nomenclature):
    if request.is_ajax():
        columns = ["", "Item", "Quantity", "Unit"]
        data = get_default_data(request, "nomenclature-detail-%s" % pk_nomenclature, NomenclatureDetailForm, None,
                                columns, "research/api/nomenclature-detail/%s/" % pk_nomenclature, 1,
                                initial_form_add={"nomenclature": pk_nomenclature})
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "item.title"},
            {"data": "quantity"},
            {"data": "unit.symbol"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
