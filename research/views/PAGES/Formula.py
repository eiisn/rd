from research.forms import FormulaForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def formula_index(request):
    if request.is_ajax():
        columns = ["", "Title", "Composition", "Memo"]
        data = get_default_data(request, "formula", FormulaForm,
                                None, columns, "research/api/formula/", 1, [2])
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
            {"data": "composition"},
            {"data": "memo"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
