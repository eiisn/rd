from research.models import Nomenclature, NomenclatureDetail
from research.serializers.Nomenclature import NomenclatureGETSerializer, NomenclaturePOSTSerializer,\
    NomenclatureDetailGETSerializer, NomenclatureDetailPOSTSerializer
from utils.BaseView import ERPView
from rest_framework import status
from rest_framework.response import Response


class NomenclatureView(ERPView):

    model = Nomenclature
    post_serializer = NomenclaturePOSTSerializer
    get_serializer = NomenclatureGETSerializer


class NomenclatureDetailView(ERPView):

    model = NomenclatureDetail
    post_serializer = NomenclatureDetailPOSTSerializer
    get_serializer = NomenclatureDetailGETSerializer

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                nomenclature_detail = NomenclatureDetail.objects.get(pk=pk)
                serialize_data = NomenclatureDetailGETSerializer(nomenclature_detail)
            except NomenclatureDetail.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)
        else:
            pk_nomenclature = kwargs.get('pk_nomenclature', None)
            if pk_nomenclature:
                nomenclature_detail = NomenclatureDetail.objects.filter(nomenclature__pk=pk_nomenclature)
                serialize_data = NomenclatureDetailGETSerializer(nomenclature_detail, many=True)
            else:
                return Response({"data": "Need pk_nomenclature"}, status=status.HTTP_404_NOT_FOUND)
        return Response({"data": serialize_data.data})

    def post(self, request, *args, **kwargs):
        pk_nomenclature = kwargs.get('pk_nomenclature', None)
        if pk_nomenclature:
            data = {"nomenclature": pk_nomenclature}
            serializer = NomenclatureDetailPOSTSerializer(data=request.data, **data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": "Need pk_nomenclature"}, status=status.HTTP_404_NOT_FOUND)
