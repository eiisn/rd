from research.models import Container
from research.serializers.Container import ContainerGETSerializer, ContainerPOSTSerializer
from utils.BaseView import ERPView
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser


class ContainerView(ERPView):

    model = Container
    post_serializer = ContainerPOSTSerializer
    get_serializer = ContainerGETSerializer
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)
