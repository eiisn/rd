from research.models import Scale, ScaleDetail
from research.serializers.Scale import ScaleGETSerializer, ScalePOSTSerializer,\
    ScaleDetailGETSerializer, ScaleDetailPOSTSerializer
from utils.BaseView import ERPView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser


class ScaleView(ERPView):

    model = Scale
    post_serializer = ScalePOSTSerializer
    get_serializer = ScaleGETSerializer


class ScaleDetailView(ERPView):

    model = ScaleDetail
    post_serializer = ScaleDetailPOSTSerializer
    get_serializer = ScaleDetailGETSerializer
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                scale_detail = ScaleDetail.objects.get(pk=pk)
                serialize_data = ScaleDetailGETSerializer(scale_detail)
            except ScaleDetail.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)
        else:
            pk_scale = kwargs.get('pk_scale', None)
            if pk_scale:
                scale_detail = ScaleDetail.objects.filter(scale__pk=pk_scale)
                serialize_data = ScaleDetailGETSerializer(scale_detail, many=True)
            else:
                return Response({"data": "Need pk_scale"}, status=status.HTTP_404_NOT_FOUND)
        return Response({"data": serialize_data.data})

    def post(self, request, *args, **kwargs):
        pk_scale = kwargs.get('pk_scale', None)
        if pk_scale:
            data = {"scale": pk_scale}
            serializer = ScaleDetailPOSTSerializer(data=request.data, **data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({"data": "Need pk_scale"}, status=status.HTTP_404_NOT_FOUND)
