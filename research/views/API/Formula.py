from research.models import Formula
from research.serializers.Formula import FormulaGETSerializer, FormulaPOSTSerializer
from utils.BaseView import ERPView
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser


class FormulaView(ERPView):

    model = Formula
    post_serializer = FormulaPOSTSerializer
    get_serializer = FormulaGETSerializer
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)
