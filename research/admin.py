from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Formula)
admin.site.register(Nomenclature)
admin.site.register(NomenclatureDetail)
admin.site.register(Container)
admin.site.register(Scale)
admin.site.register(ScaleDetail)

