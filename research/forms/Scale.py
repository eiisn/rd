from django import forms
from research.models import Scale, ScaleDetail
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field


class ScaleForm(forms.ModelForm):

    class Meta:
        model = Scale
        exclude = ('id', )


class ScaleDetailForm(forms.ModelForm):
    class Meta:
        model = ScaleDetail
        exclude = ('id', )

    def __init__(self, *args, **kwargs):
        super(ScaleDetailForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('scale', disabled=True),
            Field('num'),
            Field('item'),
            Field('step')
        )
