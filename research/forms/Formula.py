from django import forms
from research.models import Formula


class FormulaForm(forms.ModelForm):

    class Meta:
        model = Formula
        exclude = ('id', )
