from django import forms
from research.models import Nomenclature, NomenclatureDetail
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field


class NomenclatureForm(forms.ModelForm):

    class Meta:
        model = Nomenclature
        exclude = ('id', )


class NomenclatureDetailForm(forms.ModelForm):
    class Meta:
        model = NomenclatureDetail
        exclude = ('id', )

    def __init__(self, *args, **kwargs):
        super(NomenclatureDetailForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('nomenclature', disabled=True),
            Field('item'),
            Field('quantity'),
            Field('unit')
        )
