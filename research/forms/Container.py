from django import forms
from research.models import Container


class ContainerForm(forms.ModelForm):

    class Meta:
        model = Container
        exclude = ('id', )
