"""django_rd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from research.views.API import *


urlpatterns = [
    path('formula/', FormulaView.as_view()),
    path('formula/<int:pk>/', FormulaView.as_view()),
    path('nomenclature/', NomenclatureView.as_view()),
    path('nomenclature/<int:pk>/', NomenclatureView.as_view()),
    path('nomenclature-detail/<int:pk_nomenclature>/', NomenclatureDetailView.as_view()),
    path('nomenclature-detail/<int:pk_nomenclature>/<int:pk>/', NomenclatureDetailView.as_view()),
    path('container/', ContainerView.as_view()),
    path('container/<int:pk>/', ContainerView.as_view()),
    path('scale/', ScaleView.as_view()),
    path('scale/<int:pk>/', ScaleView.as_view()),
    path('scale-detail/<int:pk_scale>/', ScaleDetailView.as_view()),
    path('scale-detail/<int:pk_scale>/<int:pk>/', ScaleDetailView.as_view())
]
