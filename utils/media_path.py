"""
Fichier remplis d'utilitaire pour l'application
"""
import datetime


def user_directory_path(instance, filename):
    """
    Permet de generer le chemin de stockage des fichiers
    :param instance: instance d'un File
    :param filename: nom du fichier
    :return: chemin d'acces/sauvegarde d'une PieceJointe
    """
    from ged.models import Document
    date_fichier = datetime.date.today().strftime("%Y%m%d")
    id_fichier = Document.objects.filter(author=instance.author).count()
    #print(date_fichier, id_fichier, filename)
    return 'user/%s/%s/%s_%s' % (
        instance.author.id,
        date_fichier,
        id_fichier,
        filename
    )
