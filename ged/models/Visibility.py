from django.db import models


class Visibility(models.Model):
    title = models.CharField(max_length=25)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title
