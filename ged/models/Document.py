from django.db import models
from django.conf import settings
from utils.media_path import user_directory_path
from datetime import date, timedelta


class Document(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default=settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=200)
    type = models.CharField(max_length=200, null=True, blank=True)
    add_date = models.DateField(auto_now_add=True)
    change_date = models.DateField(auto_now=True)
    limit_date = models.DateField(default=(date.today() + timedelta(180)))
    visibility = models.ForeignKey("ged.Visibility", models.SET_NULL, null=True)
    piece = models.FileField(upload_to=user_directory_path, null=True, blank=True, default='')

    @property
    def piece_name(self):
        return self.piece

    def __str__(self):
        return self.title + " - " + str(self.author)
