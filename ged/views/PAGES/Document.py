from ged.forms.Document import DocumentForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def document_index(request):
    if request.is_ajax():
        columns = ["", "Author", "Title", "Type", "Add Date", "Last Update", "Limit Date", "Visibility", "File"]
        data = get_default_data(request, "document", DocumentForm, None, columns, "ged/api/document/", 1, [8])
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "author.username"},
            {"data": "title"},
            {"data": "type"},
            {"data": "add_date"},
            {"data": "change_date"},
            {"data": "limit_date"},
            {"data": "visibility.title"},
            {"data": "piece"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
