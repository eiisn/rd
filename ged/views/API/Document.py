from django.db.models import Q
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.response import Response
from ged.models import Document, Visibility
from ged.serializers.Document import DocumentGETSerializer, DocumentPOSTSerializer
from utils.BaseView import ERPView


class DocumentView(ERPView):

    model = Document
    post_serializer = DocumentPOSTSerializer
    get_serializer = DocumentGETSerializer
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            try:
                doc = self.model.objects.get(pk=pk)
                serialize_data = self.get_serializer(doc)
            except self.model.DoesNotExist:
                return Response({"data": "Not found id=%s" % pk}, status=status.HTTP_404_NOT_FOUND)
        else:
            doc = self.model.objects.filter(
                Q(author=request.user) | Q(visibility=Visibility.objects.get(title='Public'))
            )
            serialize_data = self.get_serializer(doc, many=True)
        return Response({"data": serialize_data.data})
