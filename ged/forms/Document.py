from django import forms
from ged.models import Document
from django.forms.widgets import FileInput


class DocumentForm(forms.ModelForm):

    class Meta:
        model = Document
        exclude = ('id', )
        widgets = {
            'piece': FileInput(),
        }
