from rest_framework import serializers
from ged.models import Document
from datetime import datetime


class DocumentGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Document
        fields = '__all__'
        depth = 2


class DocumentPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Document
        fields = '__all__'

