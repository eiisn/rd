from django.contrib import admin
from ged.models import *


# Register your models here.
admin.site.register(Document)
admin.site.register(Visibility)
