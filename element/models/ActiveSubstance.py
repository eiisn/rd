from django.db import models

import element.models
from .Element import Element


class ActiveSubstance(Element):
    origin = models.ForeignKey('element.Origin', models.SET_NULL, null=True)
    molecule = models.ManyToManyField('element.Molecule')
    aim = models.ManyToManyField('element.Aim')

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title







