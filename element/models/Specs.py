from django.db import models


class Origin (models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Molecule (models.Model):
    name = models.CharField(max_length=250)
    symbol = models.CharField(max_length=100)

    def __str__(self):
        return self.symbol


class Aim(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title


class Role(models.Model):
    title = models.CharField(max_length=200)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title


class Effect(models.Model):
    title = models.CharField(max_length=200)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title
