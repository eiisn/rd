from django.db import models
from .Element import Element


class Excipient(Element):
    role = models.ForeignKey('element.Role', models.SET_NULL, null=True)
    category = models.ForeignKey("ExcipientCategory", models.SET_NULL, 'excipient_category', null=True)
    effects = models.ManyToManyField("element.Effect")

    def __str__(self):
        return self.title

