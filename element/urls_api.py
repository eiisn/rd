"""django_rd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from element.views.API import *


urlpatterns = [
    path('substance/', ActiveSubstanceView.as_view()),
    path('substance/<int:pk>/', ActiveSubstanceView.as_view()),
    path('origin/', OriginView.as_view()),
    path('origin/<int:pk>/', OriginView.as_view()),
    path('molecule/', MoleculeView.as_view()),
    path('molecule/<int:pk>/', MoleculeView.as_view()),
    path('aim/', AimView.as_view()),
    path('aim/<int:pk>/', AimView.as_view()),
    path('excipient/', ExcipientView.as_view()),
    path('excipient/<int:pk>/', ExcipientView.as_view()),
    path('excipient-category/', ExcipientCategoryView.as_view()),
    path('excipient-category/<int:pk>/', ExcipientCategoryView.as_view()),
    path('role/', RoleView.as_view()),
    path('role/<int:pk>/', RoleView.as_view()),
    path('effect/', EffectView.as_view()),
    path('effect/<int:pk>/', EffectView.as_view()),
]
