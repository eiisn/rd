from django import forms
from element.models import Origin, Molecule, Aim, Role, Effect


class OriginForm(forms.ModelForm):

    class Meta:
        model = Origin
        exclude = ('id', )


class MoleculeForm(forms.ModelForm):

    class Meta:
        model = Molecule
        exclude = ('id', )


class AimForm(forms.ModelForm):

    class Meta:
        model = Aim
        exclude = ('id', )


class RoleForm(forms.ModelForm):

    class Meta:
        model = Role
        exclude = ('id', )


class EffectForm(forms.ModelForm):

    class Meta:
        model = Effect
        exclude = ('id', )
