from django import forms
from element.models import ExcipientCategory


class ExcipientCategoryForm(forms.ModelForm):

    class Meta:
        model = ExcipientCategory
        exclude = ('id', )
