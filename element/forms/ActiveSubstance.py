from django import forms
from element.models import ActiveSubstance


class ActiveSubstanceForm(forms.ModelForm):

    class Meta:
        model = ActiveSubstance
        exclude = ('id', )
