from django import forms
from element.models import Excipient


class ExcipientForm(forms.ModelForm):

    class Meta:
        model = Excipient
        exclude = ('id', )
