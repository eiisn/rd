from rest_framework import serializers
from element.models import Origin, Molecule, Aim, Role, Effect


class OriginGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Origin
        fields = '__all__'


class OriginPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Origin
        exclude = ['id']


class MoleculeGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Molecule
        fields = '__all__'


class MoleculePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Molecule
        exclude = ['id']


class AimGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Aim
        fields = '__all__'


class AimPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Aim
        exclude = ['id']


class RoleGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = '__all__'


class RolePOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        exclude = ['id']


class EffectGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = Effect
        fields = '__all__'


class EffectPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = Effect
        exclude = ['id']
