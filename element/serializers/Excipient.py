from rest_framework import serializers
from element.models import Excipient, Effect, ExcipientCategory


class ExcipientGETSerializer(serializers.ModelSerializer):

    effects = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='title'
     )

    class Meta:
        model = Excipient
        fields = '__all__'
        depth = 2


class ExcipientPOSTSerializer(serializers.ModelSerializer):

    effects = serializers.SlugRelatedField(
        many=True,
        queryset=Effect.objects.all(),
        slug_field='id'
     )

    class Meta:
        model = Excipient
        exclude = ['id']