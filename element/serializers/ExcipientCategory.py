from rest_framework import serializers
from element.models import ExcipientCategory


class ExcipientCategoryGETSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExcipientCategory
        fields = '__all__'


class ExcipientCategoryPOSTSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExcipientCategory
        exclude = ['id']

