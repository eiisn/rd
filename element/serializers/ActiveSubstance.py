from rest_framework import serializers
from element.models import ActiveSubstance, Molecule, Aim


class ActiveSubstanceGETSerializer(serializers.ModelSerializer):

    molecule = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='symbol'
    )

    aim = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='title'
    )

    class Meta:
        model = ActiveSubstance
        fields = '__all__'
        depth = 2


class ActiveSubstancePOSTSerializer(serializers.ModelSerializer):

    molecule = serializers.SlugRelatedField(
        many=True,
        queryset=Molecule.objects.all(),
        slug_field='id'
    )

    aim = serializers.SlugRelatedField(
        many=True,
        queryset=Aim.objects.all(),
        slug_field='id'
    )

    class Meta:
        model = ActiveSubstance
        exclude = ['id']

