"""URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from element.views.PAGES import *


urlpatterns = [
    path('get-substance-index/', active_substance_index, name='substance-index'),
    path('get-origin-index/', origin_index, name='origin-index'),
    path('get-molecule-index/', molecule_index, name='molecule-index'),
    path('get-aim-index/', aim_index, name='aim-index'),
    path('get-excipient-index/', excipient_index, name='excipient-index'),
    path('get-excipient-category-index/', excipient_category_index, name='excipient-category-index'),
    path('get-role-index/', role_index, name='role-index'),
    path('get-effect-index/', effect_index, name='effect-index'),
]
