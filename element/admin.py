from django.contrib import admin
from .models import *

admin.site.register(ActiveSubstance)
admin.site.register(Excipient)
admin.site.register(Origin)
admin.site.register(Molecule)
admin.site.register(Aim)
admin.site.register(Role)
admin.site.register(Effect)
