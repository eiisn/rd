from element.models import Excipient
from element.serializers.Excipient import ExcipientGETSerializer, ExcipientPOSTSerializer
from utils.BaseView import ERPView


class ExcipientView(ERPView):

    model = Excipient
    post_serializer = ExcipientPOSTSerializer
    get_serializer = ExcipientGETSerializer


