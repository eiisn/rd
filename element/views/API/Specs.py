from element.models import Origin, Molecule, Aim, Role, Effect
from element.serializers.Specs import OriginGETSerializer, OriginPOSTSerializer, MoleculeGETSerializer,\
    MoleculePOSTSerializer, AimGETSerializer, AimPOSTSerializer,\
    RoleGETSerializer, RolePOSTSerializer, EffectGETSerializer, EffectPOSTSerializer
from utils.BaseView import ERPView


class OriginView(ERPView):

    model = Origin
    post_serializer = OriginPOSTSerializer
    get_serializer = OriginGETSerializer


class MoleculeView(ERPView):

    model = Molecule
    post_serializer = MoleculePOSTSerializer
    get_serializer = MoleculeGETSerializer


class AimView(ERPView):

    model = Aim
    post_serializer = AimPOSTSerializer
    get_serializer = AimGETSerializer


class RoleView(ERPView):

    model = Role
    post_serializer = RolePOSTSerializer
    get_serializer = RoleGETSerializer


class EffectView(ERPView):

    model = Effect
    post_serializer = EffectPOSTSerializer
    get_serializer = EffectGETSerializer
