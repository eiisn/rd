from element.models import ExcipientCategory
from element.serializers.ExcipientCategory import ExcipientCategoryGETSerializer, ExcipientCategoryPOSTSerializer
from utils.BaseView import ERPView


class ExcipientCategoryView(ERPView):

    model = ExcipientCategory
    post_serializer = ExcipientCategoryPOSTSerializer
    get_serializer = ExcipientCategoryGETSerializer
