from element.models import ActiveSubstance
from element.serializers.ActiveSubstance import ActiveSubstanceGETSerializer, ActiveSubstancePOSTSerializer
from utils.BaseView import ERPView


class ActiveSubstanceView(ERPView):

    model = ActiveSubstance
    post_serializer = ActiveSubstancePOSTSerializer
    get_serializer = ActiveSubstanceGETSerializer


