from element.forms import ActiveSubstanceForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def active_substance_index(request):
    if request.is_ajax():
        columns = ["", "Title", "Origin", "Molecule", "Aim"]
        data = get_default_data(request, "substance", ActiveSubstanceForm,
                                None, columns, "element/api/substance/", 1, [3, 4],
                                title='Actives substances')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
            {"data": "origin.title"},
            {"data": "molecule"},
            {"data": "aim"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
