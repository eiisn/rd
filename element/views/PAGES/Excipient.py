from element.forms import ExcipientForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def excipient_index(request):
    if request.is_ajax():
        columns = ["", "Title", "Role", "Category", "Effects"]
        data = get_default_data(request, "excipient", ExcipientForm,
                                None, columns, "element/api/excipient/", 1, [4],
                                title='Excipients, Solvents & Additives')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
            {"data": "role.title"},
            {'data': 'category.name'},
            {"data": "effects"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()

