from element.forms import OriginForm, MoleculeForm, AimForm, RoleForm, EffectForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def origin_index(request):
    if request.is_ajax():
        columns = ["", "Title"]
        data = get_default_data(request, "origin", OriginForm, None, columns, "element/api/origin/", 1, title='Origins')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def molecule_index(request):
    if request.is_ajax():
        columns = ["", "Name", "Symbol"]
        data = get_default_data(request, "molecule", MoleculeForm, None, columns, "element/api/molecule/", 1,
                                title='Molecules')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "name"},
            {"data": "symbol"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def aim_index(request):
    if request.is_ajax():
        columns = ["", "Title"]
        data = get_default_data(request, "aim", AimForm, None, columns, "element/api/aim/", 1, title='Aims')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def role_index(request):
    if request.is_ajax():
        columns = ["", "Title"]
        data = get_default_data(request, "role", RoleForm, None, columns, "element/api/role/", 1)
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()


def effect_index(request):
    if request.is_ajax():
        columns = ["", "Title"]
        data = get_default_data(request, "effect", EffectForm, None, columns, "element/api/effect/", 1, title='Roles')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "title"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
