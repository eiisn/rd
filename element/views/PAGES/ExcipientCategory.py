from element.forms import ExcipientCategoryForm
from utils import get_default_data
from django.http import JsonResponse, HttpResponseForbidden


def excipient_category_index(request):
    if request.is_ajax():
        columns = ["", "Name"]
        data = get_default_data(request, "excipient-category", ExcipientCategoryForm,
                                None, columns, "element/api/excipient-category/", 1,
                                title='Excipient/solvent categories')
        data["options"]["columns"] = data["options"]["columns"] + [
            {"data": "name"},
        ]
        return JsonResponse(data)
    return HttpResponseForbidden()
